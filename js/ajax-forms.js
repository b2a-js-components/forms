var ajaxForms = (function () {

    return {
        initialize: function (selector, gotoPage) {

            
            return {
              handleSubmission : function (form, completionCallback) {

                  let linkTarget = form.getAttribute("action");
                  let linkMethod = form.getAttribute("method");

                  // let queryString = $(form).serialize();
                  let data = {};
                  let object = {};
                  let formData = new FormData(form);
                  formData.forEach((value, key) => {
                      // Reflect.has in favor of: object.hasOwnProperty(key)
                      if(!Reflect.has(object, key)){
                          object[key] = value;
                          return;
                      }
                      if(!Array.isArray(object[key])){
                          object[key] = [object[key]];
                      }
                      object[key].push(value);
                  });
                  
                  $.ajax({
                      url: linkTarget,
                      data: JSON.stringify(object),
                      method: linkMethod,
                      dataType: "json",
                      contentType : 'application/json',
                      beforeSend: function (xhr) {
                          xhr.setRequestHeader('X-Exclude-Chrome', 'true');
                      }
                  }).done(function (data, textStatus, jqXHR) {
                      completionCallback(null, jqXHR);
                      gotoPage();

                  }).fail(function (jqXHR) {
                      completionCallback(jqXHR.responseText || "ERROR", jqXHR);
                  });

                  return false;
              }   
                
            };
        }
    };
})();